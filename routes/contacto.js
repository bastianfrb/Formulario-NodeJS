var express = require('express');
var router = express.Router();
var nodemailer = require('nodemailer');

/* GET Contact Page. */
router.get('/', function(req, res, next) {
  res.render('contacto', { title: 'Contacto' });
});

/* Form de Contacto */
router.post('/enviar', function(req, res, next){
	var transporte = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: 'pruebas.bfr@gmail.com',
			pass: 'Bastian.123'
		}
	});

	var mailOpt = {
		from: 'Cristian Vega <cvega@correo.com>',
		to: 'pruebas.bfr@gmail.com',
		subject: 'Formulario de Contacto',
		text: 'Se ha enviado un mensaje desde le formulario de contacto '
				+ 'Nombre: ' + req.body.nombre
				+ ' Email: ' + req.body.email
				+ ' Mensaje: ' + req.body.mensaje,
		html: '<p>Se ha enviado un mensaje desde le formulario de contacto </p> <ul>'
				+ '<li>Nombre: ' + req.body.nombre + '</li>'
				+ '<li>Email: ' + req.body.email  + '</li>'
				+ '<li>Mensaje: ' + req.body.mensaje  + '</li>'
	};

	transporte.sendMail(mailOpt, function(error, info){
		if(error){
			console.log(error);
			res.redirect('/');
		}else{
			console.log("Mensaje enviado " + info.response);
			res.redirect('/');
		}
	});
});

module.exports = router;
